<?php
class CrmController extends Controller {
    public $layout = '//layouts/main';

    public function filters() {
        return array('accessControl');
    }

    public function accessRules() {
        return array(
            array(
                'allow',
                'actions' => array(
                    'index',
                    'view',
                ),
                'roles'   => array(
                    'Администратор',
                ),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    public function init() {
        parent::init();

        Yii::app()->theme = 'crm';
    }

}