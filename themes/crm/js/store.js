try {
    var homeUrl = homeUrl;
} catch (e) {
    homeUrl = '/';
}

function selectionChanged() {
    var selected = $('div#store-grid .selected');
    var frame = $('#frame');
    if (!selected.length) {
        frame.html('');
        return false;
    }

    var id = selected.data('id');
    frame.load(homeUrl + 'crm/store/update?id=' + id);
    return true;
}

$(document).ready(function () {
    $('#store-create').click(function (e) {
        e.preventDefault();

        $('#frame').load($(this).attr('href') + '?category=' + $('#store-grid select[name="Store[category_id]"]').val());
    });
});