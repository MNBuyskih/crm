$(document).ready(function () {
    $('#news-create').click(function (e) {
        e.preventDefault();
        $('#frame').load($(this).attr('href'), function () {
            initEditor()
        });
    });

    $('#news-form').live('submit', function (e) {
        e.preventDefault();

        var $this = $(this);
        $.ajax({
            url: $this.attr('action'),
            type: $this.attr('method'),
            data: $this.serialize(),
            success: function (html) {
                $('#frame').html(html);
                $.fn.yiiGridView.update('news-grid');
            }
        });
    })
});

function selectionChanged() {
    var selection = $('#news-grid tr.selected');

    if (!selection.length) {
        $('#frame').html('');
        return;
    }

    $.ajax({
        url: '/crm/news/update?id=' + selection.data('id'),
        success: function (html) {
            $('#frame').html(html);
            initEditor()
        }
    })
}

function initEditor() {
    CKEDITOR.replace('News[announce]', {
        toolbar: 'Standard'
    });
    CKEDITOR.replace('News[text]', {
        toolbar: 'Standard'
    });
}