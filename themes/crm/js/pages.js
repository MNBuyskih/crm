$(document).ready(function () {
    $('#addPage').click(function (e) {
        e.preventDefault();
        $('#frame').load($(this).attr('href'), function () {
            initEditor();
        });
    });

    $('#pages-form').live('submit', function (e) {
        e.preventDefault();

        var $this = $(this);
        $.ajax({
            url: $this.attr('action'),
            dataType: 'html',
            data: $this.serialize(),
            type: 'post',
            success: function (data) {
                $('#frame').html(data);
                initEditor();
            }
        });
    });
});

function initEditor() {
    CKEDITOR.replace('Pages[text]', {
        toolbar: 'Standard',
        allowedContent: "a[class,!href](btn, btn-primary);p;hr;table[border,cellpadding,cellspacing]{width};tr;td{width,align,vertical-align};th;tbody;tfooter;strong;em;i;b;u;del;sub;sup;img[!src,!alt];ul;li"
    });
}

function selectionChanged() {
    var tr = $('#pages-grid tr.selected');

    if (!tr.length) {
        $('#frame').html('');
        return;
    }

    $.ajax({
        url: '/crm/pages/update?id=' + tr.data('id'),
        success: function (data) {
            $('#frame').html(data);
            initEditor();
        }
    });
}