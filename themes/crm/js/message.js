$(document).ready(function () {
    $('#message-category-create').live('click', function (e) {
        e.preventDefault();

        $('#frame').load($(this).attr('href'));
    });

    $('#message-category-form').live('submit', function (e) {
        e.preventDefault();

        var $this = $(this);

        $.ajax({
            url: $this.attr('action'),
            type: $this.attr('method'),
            data: $this.serialize(),
            success: function (html) {
                $('#frame').html(html);
                $.fn.yiiGridView.update('message-category-grid');
            }
        });
    });

    $('#message-form').live('submit', function (e) {
        e.preventDefault();

        var $this = $(this);

        $.ajax({
            url: $this.attr('action'),
            type: $this.attr('method'),
            data: $this.serialize(),
            success: function (html) {
                $('#frame').html(html);
                initEditor();
            }
        });
    });
});

function selectionChanged() {
    var tr = $('#message-category-grid tr.selected');
    if (!tr.length) {
        $('#frame').html('');
        return;
    }

    var id = tr.data('id');
    $('#frame').load('/crm/message/update?id=' + id);
}

function addMessage(a) {
    var id = $(a).closest('tr').data('id');

    $('#frame').load('/crm/message/add?category=' + id, function () {
        initEditor();
    });
}

function initEditor() {
    CKEDITOR.replace('Message[body]', {
        toolbar: 'Standard'
    });
}

function messageSelected() {
    var tr = $('#message-grid tr.selected');
    if (!tr.length) {
        $('#frame').html('');
        return;
    }

    var id = tr.data('id');
    $('#frame').load('/crm/message/messageUpdate?id=' + id, function () {
        initEditor();
    });
}