function selectionChanged(id) {
    var table = $('#' + id);
    var frame = $('#frame');
    var selection = $(table).yiiGridView('getSelection');
    if (!selection.length) {
        frame.html('');
        return;
    }
    selection = selection[0];
    frame.load('/crm/users/view?id=' + selection);
}

$('#user-form').live('submit', function (e) {
    e.preventDefault();

    var role = $('#UserForm_role').val();
    if ((role == 2 || role == 3) && !confirm('Вы уверены, что хотите дать пользователю особые права?')) {
        return;
    }

    $.ajax({
        url: $(this).attr('action'),
        data: $(this).serialize(),
        type: $(this).attr('method'),
        dataType: 'html',
        success: function (html) {
            $('#frame').html(html);
        }
    });
});

function privateMessage(a) {
    if (a.tagName != 'A') a = $(a).closest('a');
    $('#frame').load($(a).attr('href'), function () {
        initEditor();
    });
}

function addBalance(a) {
    if (a.tagName != 'A') a = $(a).closest('a');
    $('#frame').load($(a).attr('href'));
}

function initEditor() {
    CKEDITOR.replace('Message[body]', {
        toolbar: 'Standard'
    });
}

$('#message-form').live('submit', function (e) {
    e.preventDefault();

    $.ajax({
        url: $(this).attr('action'),
        data: $(this).serialize(),
        type: $(this).attr('method'),
        dataType: 'html',
        success: function (html) {
            $('#frame').html(html);
            initEditor();
        }
    });
});

$('#balance-form').live('submit', function (e) {
    e.preventDefault();

    $.ajax({
        url: $(this).attr('action'),
        data: $(this).serialize(),
        type: $(this).attr('method'),
        dataType: 'html',
        success: function (html) {
            $('#frame').html(html);
        }
    });
});

$('.user-info a').live('click', function (e) {
    e.preventDefault();

    $('.user-info div').slideToggle();
})