$(document).ready(function () {
    $('#create-discount').click(function (e) {
        e.preventDefault();

        $('#frame').load($(this).attr('href'));
    });

    $('#discount-form').live('submit', function (e) {
        e.preventDefault();

        var $this = $(this);
        $.ajax({
            url: $this.attr('action'),
            type: 'post',
            data: $this.serialize(),
            success: function (data) {
                $('#frame').html(data);
                $.fn.yiiGridView.update('discounts-grid');
            }
        });
    });
});

function selectionChanged() {
    var tr = $('#discounts-grid tr.selected');

    if (!tr.length) {
        $('#frame').html('');
    }

    var id = tr.data('id');
    $('#frame').load('/crm/discounts/update?id=' + id);
}