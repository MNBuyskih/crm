try {
    var homeUrl = homeUrl;
} catch (e) {
    homeUrl = '/';
}

function selectionChanged() {
    var selected = $('div#storeCategory-grid tr.selected');
    var frame = $('#frame');

    if (!selected.length) {
        frame.html('');
        return false;
    }

    var id = selected.data('id');
    frame.load(homeUrl + 'crm/storeCategory/update?id=' + id);
    return true;
}

$(document).ready(function () {
    $('#storeCategory-create').click(function (e) {
        e.preventDefault();

        $('#frame').load($(this).attr('href'));
    });

    $('#storeCategory-form').live('submit', function (e) {
        e.preventDefault();

        var $form = $(this);
        $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            type: 'post',
            dataType: 'html',
            success: function (data) {
                $('#frame').html(data);
                $.fn.yiiGridView.update('storeCategory-grid');
            }
        });
    });
});
