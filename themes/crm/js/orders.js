$(document).ready(function () {
    $('a.change-state').live('click', function () {
        $('.change-state-cont').slideToggle();
        return false;
    });

    $('#action-form input[type="submit"]').live('click', function () {
        $('#action-form').find('input[name="state"]').val($(this).attr('name'));
    });

    $('#cart-form').live('submit', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        var id = $('#orders-grid tr.selected').data('id');
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            dataType: 'html',
            data: data,
            success: function (html) {
                $('#orders-grid-wrap').load(document.location.href + ' #orders-grid', function () {
                    $('#orders-grid tr[data-id="' + id + '"]').addClass('selected');
                });
                $('#frame').html(html);
            },
            error: function (xhr) {
                alert('Сервер вернул ошибку:\n' + xhr.responseText);
            }
        });
    });

    $('#action-form a.delete').live('click', function (e) {
        e.preventDefault();
        if (!confirm('Подтвердите удаление заказа')) {
            return;
        }
        $.ajax({
            url: $(this).attr('href'),
            type: 'get',
            dataType: 'html',
            success: function (html) {
                $('#frame').html(html);
                $.fn.yiiGridView.update("orders-grid");
            },
            error: function (xhr) {
                alert('Сервер вернул ошибку:\n' + xhr.responseText);
            }
        });
    });

    $('#orders-grid tr').live('click', function (e) {
        if (e.target.tagName == 'A') return true;

        e.preventDefault();

        var $this = $(this);
        var active = $this.hasClass('selected');
        $('#orders-grid tr').removeClass('selected');
        if (!active) {
            $this.addClass('selected');
            $('#frame').load('/crm/orders/view?id=' + $this.data('id'), function () {
                $('body,html').animate({
                    scrollTop: $('.right').position().top
                });
            });
        } else {
            $('#frame').html('');
        }

    });
});

