<?
/**
 * @var $this    CrmController
 * @var $content string
 */
Yii::app()->clientScript->registerScript('home-url', 'var homeUrl ="' . Yii::app()->homeUrl . '"', CClientScript::POS_HEAD);
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<?= Yii::app()->theme->baseUrl ?>/css/style.css"/>
    <title><?= CHtml::encode($this->pageTitle) ?></title>
</head>
<body>
<div class="wrap">
    <div id="menu">
        <?$this->widget('zii.widgets.CMenu', array(
                                                  'items' => array(
                                                      array(
                                                          'label'   => 'Заказы',
                                                          'url'     => array('/crm'),
                                                          'visible' => Yii::app()->user->checkAccess(array(
                                                                                                          'Администратор',
                                                                                                          'Менеджер'
                                                                                                     )),
                                                      ),
                                                      array(
                                                          'label'   => 'Пользователи',
                                                          'url'     => array('/crm/users'),
                                                          'visible' => Yii::app()->user->checkAccess('Администратор'),
                                                      ),
                                                      array(
                                                          'label'   => 'Сообщения',
                                                          'url'     => array('/crm/message'),
                                                          'visible' => Yii::app()->user->checkAccess('Администратор'),
                                                      ),
                                                      array(
                                                          'label'   => 'Скидки',
                                                          'url'     => array('/crm/discounts'),
                                                          'visible' => Yii::app()->user->checkAccess('Администратор'),
                                                      ),
                                                      array(
                                                          'label'   => 'Новости',
                                                          'url'     => array('/crm/news'),
                                                          'visible' => Yii::app()->user->checkAccess('Администратор'),
                                                      ),
                                                      array(
                                                          'label'   => 'Страницы',
                                                          'url'     => array('/crm/pages'),
                                                          'visible' => Yii::app()->user->checkAccess('Администратор'),
                                                      ),
                                                      array(
                                                          'label'   => 'Магазин',
                                                          'url'     => array('/crm/store'),
                                                          'visible' => Yii::app()->user->checkAccess('Администратор'),
                                                      ),
                                                      array(
                                                          'label'   => 'Параметры',
                                                          'url'     => array('/crm/params'),
                                                          'visible' => Yii::app()->user->checkAccess('Администратор'),
                                                      ),
                                                  )
                                             ))?>
        <div class="return">
            <?$this->widget('zii.widgets.CMenu', array(
                                                      'items' => array(
                                                          array(
                                                              'label' => Users::currentUser()->username . ' (' . Users::getRole(Users::currentUser()->role) . ')',
                                                          ),
                                                          array(
                                                              'label' => 'Выход',
                                                              'url'   => array('/user/default/logout')
                                                          ),
                                                          array(
                                                              'label' => 'Вернуться на сайт',
                                                              'url'   => Yii::app()->homeUrl
                                                          ),
                                                      )
                                                 ))?>
        </div>
    </div>
    <div class="left">
        <div class="inner">
            <?= $content ?>
        </div>
    </div>
    <div class="right">
        <div class="inner">
            <div id="frame"></div>
        </div>
    </div>
</div>
</body>
</html>